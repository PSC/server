import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;


public class Reception extends Thread {

    private BufferedReader in;
    private String message = null;
    private String privkey = null;
    private Emission em = null;

    public Reception(BufferedReader in, String privkey, Emission e) {
        this.in = in;
        this.privkey = privkey;
        this.em = e;
    }

    public void run() {
        while(true){
            try {
                message = in.readLine();
                // TODO décrypter le message
                if (message == null) { break; }
                String[] t = message.split(" ");
                if(t.length > 1) {
                    Commande c = new Commande(em, t);
                    c.start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

