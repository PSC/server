import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class Emission extends Thread {

    private PrintWriter out;
    private String login = null, message = null;
    private Scanner sc = null;
    private volatile boolean interrupted = false;
    private String pubkey = null; // la clé du serveur

    public Emission(PrintWriter out, String pubkey) {
        this.out = out;
        this.pubkey = pubkey;
    }

    public void run() {
        interrupted = false;
        try {
            sc = new Scanner(System.in);

            while(!this.interrupted){
                message = sc.nextLine();
                send(message);
            }
        } finally {
            System.out.println("Le serveur s'est déconnecté.");
        }
    }

    public void send(String message) {
        // TODO crypter le message à envoyer
        out.println(message);
        out.flush();
    }

    public void interrupt() {
        System.out.println("Thread émission interrompu.");
        this.interrupted=true;
    }
}

