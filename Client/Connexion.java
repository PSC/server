import java.net.*;
import java.util.Scanner;
import java.io.*;
import java.util.Formatter;
import java.security.MessageDigest;

public class Connexion extends Thread {

    private Socket socket = null;
    public static Thread t2;
    public static String login = null, pass = null, message1 = null, message2 = null, message3 = null;
    private PrintWriter out = null;
    private BufferedReader in = null;
    private Scanner sc = null;
    private boolean connect = false;

    public Connexion(Socket s) {
        socket = s;
    }

    public void run() {
        try {
            out = new PrintWriter(socket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));	
            sc = new Scanner(System.in);
            while(!connect ) {
                System.out.println(in.readLine());
                login = sc.nextLine();
                out.println(login);
                out.flush();
                String answer = in.readLine();
                if (answer.equals("erreur")) {
                    System.out.println("Login inexistant");
                    continue;
                }
                System.out.println(answer);
                pass = sc.nextLine();
                // TODO hasher le mot de passe avant de l'envoyer
                out.println(encryptPassword(pass));
                out.flush();
                if(in.readLine().equals("connecte")) {
                    System.out.println("Je suis connecté "); 
                    connect = true;
                }
                else {
                    System.err.println("Vos informations sont incorrectes "); 
                }
            }
            t2 = new Thread(new Chat_ClientServeur(socket));
            t2.start();
        } catch (IOException e) {
            System.err.println("Le serveur ne répond plus ");
        }
    }

    private static String encryptPassword(String password) {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch(Exception e) {
            e.printStackTrace();
        }
        return sha1;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}

