import java.io.*;
import java.net.*;
import java.util.Scanner;


public class Chat_ClientServeur extends Thread {

    private Socket socket;
    private PrintWriter out = null;
    private BufferedReader in = null;
    private Scanner sc;
    private Thread t3, t4;
    private String pubkey; // la clé du serveur
    private String privkey; // la clé du client

    public Chat_ClientServeur(Socket s) {
        socket = s;
    }

    public void run() {
        try {
            out = new PrintWriter(socket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            sc = new Scanner(System.in);

            Emission e = new Emission(out,pubkey);
            Thread t4 = new Thread(e);
            t4.start();
            Thread t3 = new Thread(new Reception(in, privkey, e));
            t3.start();
            // System.out.println("Thread Reception démarré.");
            t3.join();
            // System.out.println("Interruption de réception détectée.");
            e.interrupt();
            // System.out.println("Signal stop envoyé à l'Emetteur.");
        } catch (Exception e) {
            System.err.println("Le serveur distant s'est déconnecté !");
        }
    }
}

