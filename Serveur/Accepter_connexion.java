import java.io.*;
import java.net.*;


public class Accepter_connexion extends Thread {
    private ServerSocket socketserver = null;
    private Socket socket = null;
    private Emission emetteur = null;
    public Thread t1;

    public Accepter_connexion(ServerSocket ss,Emission e) {
        socketserver = ss;
        emetteur = e;
    }

    public void run() {
        try {
            while(true) {
                socket = socketserver.accept();
                System.out.println("Un client veut se connecter.");

                t1 = new Thread(new Authentification(socket,this.emetteur));
                t1.start();
            }
        } catch (IOException e) {
            System.err.println("Erreur serveur");
        }
    }
}
