import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.LinkedList;
import java.util.Random;

public class Commande extends Thread {

    private String[] arguments;
    private int index;
    private Emission e;

    public Commande(int index, Emission e, String[] arguments) {
        this.index = index;
        this.arguments = arguments;
        this.e = e;
    }

    public void run() {
        PrintWriter p;
        try {
            int value = Integer.parseInt(this.arguments[0]);
            print(value, "serveur: "+arguments[1]);
            // we are handling a response from a client
            switch(this.arguments[1]) {
                case "traceroute":
                    print(value, "server: La traceroute renvoie "+arguments[2]);
                    break;
                case "dns":
                    print(value, "server: La requête DNS renvoie "+arguments[2]);
                    break;
                case "dnsi":
                    print(value, "server: La requête DNS inverse renvoie "+arguments[2]);
                    break;
                default:
                    print(value, "server: Votre réponse est incorrecte. Voici les commandes que je comprends à l'heure actuelle : traceroute, dns, ainsi que dnsi");
                    break;
            }
        } catch (NumberFormatException nfe) {
            // we are handling a request from the client
            switch(this.arguments[0]) {
                case "print":
                    print(index, "server: "+ arguments[1]);
                    break;
                case "printa":
                    for(int k = 0; k<e.getLength(); k++) {
                        print(k, "server: "+ arguments[1]);
                    }
                    break;
                case "traceroute":
                    print(index, "server: Je vais demander à un client de lancer une traceroute");
                    traceroute(arguments[1]);
                    break;
                case "dns":
                    print(index, "server: Je vais lancer une requête DNS");
                    dns(arguments[1]);
                    break;
                case "dnsi":
                    print(index, "server: Je vais lancer une requête DNS inverse");
                    dnsi(arguments[1]);
                    break;
                default:
                    print(index, "server: Votre demande est incorrecte. Voici les commandes que je comprends à l'heure actuelle : print, printa, traceroute, dns, ainsi que dnsi");
                    break;
            }
        }
    }
    
    public void print(int index, String message) {
        PrintWriter p = e.getEmissionChannel(index);
        p.println(message);
        p.flush();
    }
    
    public void traceroute(String ip) {
        // envoyer la demande au client
        print(findOtherClient(), index+" traceroute "+ ip);
    }
    
    public void dns(String url) {
        // envoyer la demande au client
        print(findOtherClient(), index+" dns "+ url);
    }
    
    public void dnsi(String ip) {
        // envoyer la demande au client
        print(findOtherClient(), index+" dnsi "+ ip);
    }
    
    public int findOtherClient() {
        Random rand = new Random();
        int  n = rand.nextInt(e.getLength());
        System.out.println(n);
        if(n == index) {
            return findOtherClient();
        }
        else return n;
    }
}
