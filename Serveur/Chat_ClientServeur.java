import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class Chat_ClientServeur  extends Thread {

    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter out = null;
    private String login = "zero";
    private Thread t3;
    private Emission emetteur;

    public Chat_ClientServeur(Socket s, String log, Emission e) {
        socket = s;
        login = log;
        emetteur = e;
    }

    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());
            Thread t3 = new Thread(new Reception(in, emetteur.getLength(), login, emetteur));
            t3.start();
            emetteur.add(out);
        } catch (IOException e) {
            System.err.println(login +"s'est déconnecté ");
        }
    }
}
