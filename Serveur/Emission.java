import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.LinkedList;

public class Emission extends Thread {

    private LinkedList<PrintWriter> outs;
    private String message = null;
    private Scanner sc = null;
    private String login;
    private String pubkey;

    public Emission() {
        this.outs = new LinkedList<PrintWriter>();
    }

    public void run() {
        sc = new Scanner(System.in);
        while(true){
            message = sc.nextLine();
            for (PrintWriter out : this.outs) {
                out.println(message);
                out.flush();
            }
        }
    }

    public void add(PrintWriter out) {
        this.outs.add(out);
    }
    
    public int getLength() {
        return this.outs.size();
    }
    
    public PrintWriter getEmissionChannel(int index) {
        return this.outs.get(index);
    }
}
