import java.io.*;
import java.net.*;

public class Serveur {
    public static ServerSocket ss = null;
    public static Thread t;
    public static Thread t1;

    public static void main(String[] args) {
        try {
            ss = new ServerSocket(2009);
            System.out.println("Le serveur est à l'écoute du port "+ss.getLocalPort());
            Emission e = new Emission();
            t1 = new Thread(e);
            t = new Thread(new Accepter_connexion(ss, e));
            t.start();
            t1.start();
        } catch (IOException e) {
            System.err.println("Le port "+ss.getLocalPort()+" est déjà utilisé !");
        }
    }
}
