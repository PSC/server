import java.net.*;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.io.*;

public class Authentification extends Thread {
    private Socket socket;
    private PrintWriter out = null;
    private BufferedReader in = null;
    private String login = "zero", pass =  null;
    public boolean authentifier = false;
    public Thread t2;
    private Emission emetteur;
    private boolean nouveau = false;

    public Authentification(Socket s, Emission e){
        socket = s;
        emetteur = e;
    }

    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());
            while(!authentifier){
                out.println("Entrez votre login :");
                out.flush();
                login = in.readLine();
                if (login==null) { throw new IOException(); }
                String info = infoClient(login);
                if (info.equals("")) {
                    //out.println("erreur"); out.flush(); continue;
                    nouveau=true;
                }
                out.println("Entrez votre mot de passe :");
                out.flush();
                pass = in.readLine();
                if (pass==null) { throw new IOException(); }
                if(isValid(login, pass, info,nouveau)){
                    out.println("connecte");
                    System.out.println(login +" vient de se connecter ");
                    out.flush();
                    authentifier = true;
                }
                else {out.println("erreur"); out.flush();}
            }
            if (authentifier) {
                t2 = new Thread(new Chat_ClientServeur(socket, login, this.emetteur));
                t2.start();
            }
        } catch (Exception e) {
            System.err.println(login+" ne répond pas !");
        }
    }
    
    private static String infoClient(String login) {
        try {
            Scanner sc = new Scanner(new File("zero.txt"));
            while (sc.hasNext()) {
                String nl = sc.nextLine();
                if (nl.startsWith(login+" ")) {
                    return nl;
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Le fichier n'existe pas !");
        }
        return "";
    }

    private static boolean isValid(String login, String pass, String info, boolean nouveau) {
        if (nouveau) {
            try {
                System.out.println("! Création du compte de "+login);
                Files.write(Paths.get("zero.txt"), (login+" "+pass+"\n").getBytes("UTF8"), StandardOpenOption.APPEND);
            } catch (IOException e) {
                return false;
            }
            return true;
        } else {
            return info.equals(login+" "+pass);
        }
    }
}
