import java.io.BufferedReader;
import java.io.IOException;


public class Reception extends Thread {
    private BufferedReader in;
    private String message = null, login = null;
    private int index;
    private Emission e;

    public Reception(BufferedReader in, int index, String login, Emission e){
        this.in = in;
        this.login = login;
        this.index = index;
        this.e = e;
    }

    public void run() {
        while(true) {
            try {
                message = in.readLine();
                if(message == null) {
                    System.out.println(login+" s'est déconnecté");
                    break;
                }
                String[] t = message.split(" ");
                Commande c = new Commande(index, e, t);
                c.start();
                System.out.println("<"+login+"> "+message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
